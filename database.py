# pylint: disable=missing-module-docstring
import sqlite3
import pyexcel


connection = sqlite3.connect("food.db")
cursor = connection.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS foods(food_name TEXT)")

FILE_PATH = "foods.ods"
sheet = pyexcel.get_sheet(file_name=FILE_PATH)

for row in sheet:
    food_name = row[0]
    cursor.execute("INSERT INTO foods(food_name) VALUES (?)", (food_name,))

connection.commit()
connection.close()
