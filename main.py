# pylint: disable=unused-wildcard-import
# pylint: disable=wildcard-import
# pylint: disable=missing-module-docstring

import sqlite3
import random
from tkinter import *
from tkinter import ttk


def get_all_foods(database_file):
    """Retrieve all food items from the specified SQLite database file.

    Args:
        database_file (str): The path to the SQLite database file containing the 'foods' table.

    Returns:
        List: A list of tuples, where each tuple represents a row in the 'foods' table.
    """
    connection = sqlite3.connect(database_file)
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM foods")
    result = cursor.fetchall()
    connection.close()
    return result

def choose_food():
    """
    Randomly select a food item from the 'foods' table in the 'food.db' SQLite database file,
    and update the 'lunch' StringVar with the selected food.
    """
    database_file = 'food.db'
    all_foods = get_all_foods(database_file)
    all_index = []
    for amount in range(0, len(all_foods)):
        all_index.append(amount)
    random_index = random.randint(0, len(all_foods) - 1)
    selected_food = all_foods[random_index]
    food_name = selected_food[0].replace("{", "").replace("}", "")
    lunch.set(food_name)


root = Tk()
root.title("Food Choice Generator")


mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

lunch = StringVar()
ttk.Label(mainframe, textvariable=lunch).grid(column=2, row=2, sticky=(W, E))

ttk.Button(mainframe, text="Show food", command=choose_food).grid(column=3, row=3, sticky=W)
ttk.Label(mainframe, text="Our lunch today will be: ").grid(column=1, row=2, sticky=E)

for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)


root.mainloop()
