# Food Choice Generator

Food Choice Generator is a simple Python application that helps users decide what to eat for lunch by randomly selecting a food option from a SQLite database.

## Features

- Retrieves food options from a local SQLite database
- Randomly selects a food option with the click of a button
- Displays the selected food option in a user-friendly GUI using Tkinter

## Prerequisites

- Python 3.x
- sqlite3 module (included in Python standard library)

## Installation

1. Clone the repository:

    ```
    git clone https://gitlab.com/William_Cezar/food-choice-generator.git
    ```

2. Ensure that you have the `food.db` SQLite database file in the same directory as the `main.py` file. If you don't have the file yet, you can create it using the `database.py` script. Please also note that the food.db is populated using data from a `.ods` file called foods.

## Usage

Run the `main.py` file to launch the Food Choice Generator application:

    ```
    python main.py
    ```

A window will appear with a button labeled "Show food." Click the button to generate a random food option. The chosen option will be displayed in the GUI.

## Contributing

If you would like to contribute to this project, please open an issue or submit a merge request.


